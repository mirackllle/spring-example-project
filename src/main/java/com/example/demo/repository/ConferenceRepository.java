package com.example.demo.repository;

import com.example.demo.model.entity.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {
    //я вынес логику определения дубликата на уровень ДАО. Это неверный подход?
    Conference findByNameAndTopicAndDate(String name, String topic, Date date);

    Conference findByNameAndTopic(String name, String topic);

    Conference findByDate(Date date);

    Date findDateById(long id);
}
