package com.example.demo.repository;

import com.example.demo.model.entity.Talk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TalkRepository extends JpaRepository<Talk, Long> {

    int countByConferenceIdAndSpeakerName(long id, String name);

    Talk getByConferenceIdAndName(long id, String name);
}
