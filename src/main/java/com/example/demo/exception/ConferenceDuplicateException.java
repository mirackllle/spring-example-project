package com.example.demo.exception;

public class ConferenceDuplicateException extends RuntimeException {

    public ConferenceDuplicateException() {
        super();
    }

    public ConferenceDuplicateException(String message) {
        super(message);
    }
}
