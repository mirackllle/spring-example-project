package com.example.demo.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Talk {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private String speakerName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "talk_type_id")
    private TalkType talkType;

    @Column(name = "conference_id")
    private Long conferenceId;
}
