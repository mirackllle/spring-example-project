package com.example.demo.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
//@Value
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceDtoResponse {

    private Long id;
    private String name;
    private String topic;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date conferenceDate;
    private int participantsCount;
}
