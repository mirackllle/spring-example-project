package com.example.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TalkDtoResponse {

    private Long id;

    private String name;

    private String description;

    private String speakerName;

    private TalkTypeDto talkType;

}
