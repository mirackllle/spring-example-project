package com.example.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TalkDtoRequest {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String speakerName;

    @NotNull
    private TalkTypeDto talkType;

}
