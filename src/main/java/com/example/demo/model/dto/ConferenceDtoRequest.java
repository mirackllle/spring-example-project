package com.example.demo.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
//@Value
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceDtoRequest {

    @NotNull
    private String name;
    @NotNull
    private String topic;
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
//    @Future
    private Date conferenceDate;
    @NotNull
    @Min(100)
    private int participantsCount;
}
