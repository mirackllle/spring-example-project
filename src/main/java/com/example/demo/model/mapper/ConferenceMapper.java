package com.example.demo.model.mapper;

import com.example.demo.domain.ConferenceDomain;
import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.example.demo.model.dto.TalkDtoRequest;
import com.example.demo.model.dto.TalkDtoResponse;
import com.example.demo.model.entity.Conference;
import com.example.demo.model.entity.Talk;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ConferenceMapper {

    @Mapping(source = "date", target = "conferenceDate")
    ConferenceDtoResponse mapEntityToDtoResponse(Conference conference);

    @Mapping(source = "conferenceDate", target = "date")
    ConferenceDomain mapReqToDomain(ConferenceDtoRequest conferenceDtoRequest);

    @Mapping(source = "conferenceDate", target = "date")
    Conference mapDtoRequestToEntity(ConferenceDtoRequest conferenceDto);

    TalkDtoResponse mapEntityToDtoResponseTalk(Talk talk);

    List<TalkDtoResponse> mapEntityToDtoResponseTalkList(List<Talk> talk);

    Talk mapDtoRequestToEntityTalk(TalkDtoRequest talkDtoRequest);

    List<Talk> mapDtoRequestToEntityTalkList(List<TalkDtoRequest> talkDtoRequest);


}
