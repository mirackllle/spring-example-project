package com.example.demo.controller;


import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConferenceDuplicateException;
import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.example.demo.model.dto.TalkDtoRequest;
import com.example.demo.model.dto.TalkDtoResponse;
import com.example.demo.service.ConferenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ConferenceController {

    private final ConferenceService service;


    @GetMapping("/conferences")
    @ResponseStatus(value = HttpStatus.OK)
    public List<ConferenceDtoResponse> getAllConferences() {
        return this.service.getAll();
    }

    @PostMapping(value = "/conferences", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    //@VALID - мы валидируем чужую сущность? Нужно валидировать только то, что создаем мы сами!
    //Валид сместить в сервис
    //Когда нужно применять ConferenceDomainDto ? Какие критерии?
    public ConferenceDtoResponse createNewConference(@RequestBody @Valid ConferenceDtoRequest conferenceDto) {
        return service.saveConference(conferenceDto);
    }

    @PutMapping("/conferences/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ConferenceDtoResponse updateConference(@PathVariable long id, @RequestBody @Valid ConferenceDtoRequest conferenceDto) {
        return this.service.update(id, conferenceDto);
    }

    @DeleteMapping("/conferences/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteConference(@PathVariable long id) {
        this.service.delete(id);
    }

    @PostMapping("/conferences/{id}/talk")
    @ResponseStatus(value = HttpStatus.CREATED)
    public TalkDtoResponse createNewTalk (@PathVariable long id, @RequestBody @Valid TalkDtoRequest talk) {
        return this.service.saveNewTalkByConferenceId(id, talk);
    }

    @GetMapping("/conferences/{id}/talk")
    public List<TalkDtoResponse> getTalks(@PathVariable long id) {
        return service.getTalksByConferenceId(id);
    }

    @ExceptionHandler({BadRequestException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<String> handleException(BadRequestException ex) {
        return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConferenceDuplicateException.class)
    public ResponseEntity<String> handleConflictException(ConferenceDuplicateException ex) {
        return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.CONFLICT);
    }

}
