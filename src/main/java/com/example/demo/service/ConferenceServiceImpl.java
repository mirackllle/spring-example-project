package com.example.demo.service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConferenceDuplicateException;
import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.example.demo.model.dto.TalkDtoRequest;
import com.example.demo.model.dto.TalkDtoResponse;
import com.example.demo.model.entity.Conference;
import com.example.demo.model.mapper.ConferenceMapper;
import com.example.demo.repository.ConferenceRepository;
import com.example.demo.repository.TalkRepository;
import io.micrometer.core.annotation.Counted;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ConferenceServiceImpl implements ConferenceService{

    private static final int AVAILABLE_DAYS_FOR_TALK = 30;
    private final ConferenceRepository conferenceRepository;
    private final TalkRepository talkRepository;
    private final ConferenceMapper conferenceMapper;
    private final ApplicationEventPublisher eventPublisher;

    @Override
    @Counted(value = "conference.saved.count", description = "Saved conference")
    //Сервис завязан на ДТО частного адаптера. Если у меняч 5 контроллеров (с разными ДТО),
    // тогда в сервисе 5 вариаций методов saveConference ? Generic income for save?
    // Или вот тогда и выставить domainRequestDto?
    public ConferenceDtoResponse saveConference(ConferenceDtoRequest conference) {
        Conference conferenceIncome = conferenceMapper.mapDtoRequestToEntity(conference);
        // Одна процедура --- друга процедура...
        // Можно ли перенести ВАЛИДАЦИИ в доменные сущности?

        checkFreeDateForConference(conferenceIncome);
        validateConferenceOnDuplicate(conferenceIncome);

        return conferenceMapper.mapEntityToDtoResponse(this.conferenceRepository.save(conferenceIncome));
    }

    @Override
    @Counted(value = "conference.updated.count", description = "Updated conference")
    public ConferenceDtoResponse update(long id, ConferenceDtoRequest conference) {
        Conference conferenceIncome = conferenceMapper.mapDtoRequestToEntity(conference);
        conferenceIncome.setId(id);
        checkFreeDateForConference(conferenceIncome);
        validateConferenceOnDuplicate(conferenceIncome);
        return conferenceMapper.mapEntityToDtoResponse(this.conferenceRepository.save(conferenceIncome));
    }

    @Override
    @Counted(value = "conference.deleted.count", description = "Deleted conference")
    public void delete(long id) {
        this.conferenceRepository.deleteById(id);
    }

    @Override
    public List<ConferenceDtoResponse> getAll() {
        return this.conferenceRepository.findAll().stream()
                .map(this.conferenceMapper::mapEntityToDtoResponse).collect(Collectors.toList());
    }

    @Override
    public Optional<Conference> getById(long id) {
        return this.conferenceRepository.findById(id);
    }

    @Override
    //подчитать к-во вызовов методов, но без анализа бизнесом - внутренний анализ
    @Counted(value = "talk.saved.count", description = "Saved talks")
    public TalkDtoResponse saveNewTalkByConferenceId(long id, TalkDtoRequest talkDtoRequest) {
        Conference conference = getConferenceForTalksById(id);
        validateTalksCount(id, talkDtoRequest.getSpeakerName());
        validateTalkName(id, talkDtoRequest.getName());
        validateConferenceTime(conference.getDate());
        var talk = conferenceMapper.mapDtoRequestToEntityTalk(talkDtoRequest);
        talk.setConferenceId(id);
        return conferenceMapper.mapEntityToDtoResponseTalk(talkRepository.save(talk));
    }

    @Override
    public List<TalkDtoResponse> getTalksByConferenceId(long id) {
        Conference conference = getConferenceForTalksById(id);
        return conferenceMapper.mapEntityToDtoResponseTalkList(conference.getTalks());
    }

    private Conference getConferenceForTalksById(long id) {
        Optional<Conference> conference = this.conferenceRepository.findById(id);
        if(conference.isEmpty()) {
            throw new BadRequestException("Conference was not found");
        }
        return conference.get();
    }

    private void validateTalksCount(long id, String speakerName) {
        int count = talkRepository.countByConferenceIdAndSpeakerName(id, speakerName);
        if (count >= 3) {
            throw new BadRequestException("Speaker already has 3 talks");
        }
    }

    private void validateTalkName(long id, String talkName) {
        if (talkRepository.getByConferenceIdAndName(id, talkName) != null) {
            throw new ConferenceDuplicateException("Talk must have unique name");
        }
    }

    private void validateConferenceTime(Date date) {
        if (Duration.between(Instant.now(), date.toInstant()).toDays() < AVAILABLE_DAYS_FOR_TALK) {
            throw new BadRequestException("Left less than 30 days, you can't add new talk");
        }
    }

    private void checkFreeDateForConference(Conference conferenceIncome) {
        if (conferenceRepository.findByDate(conferenceIncome.getDate()) != null) {
            throw new ConferenceDuplicateException("Conference is exist!");
        }
    }

    private void validateConferenceOnDuplicate(Conference conferenceIncome) {
        if (conferenceRepository.findByNameAndTopic(conferenceIncome.getName(), conferenceIncome.getTopic()) != null) {
            throw new ConferenceDuplicateException("Conference is exist!");
        }
    }
}
