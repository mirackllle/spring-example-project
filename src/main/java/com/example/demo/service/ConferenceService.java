package com.example.demo.service;

import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.example.demo.model.dto.TalkDtoRequest;
import com.example.demo.model.dto.TalkDtoResponse;
import com.example.demo.model.entity.Conference;

import java.util.List;
import java.util.Optional;

public interface ConferenceService {

    ConferenceDtoResponse saveConference(ConferenceDtoRequest conference);

    ConferenceDtoResponse update(long id, ConferenceDtoRequest conference);

    void delete(long id);

    List<ConferenceDtoResponse> getAll();

    Optional<Conference> getById(long id);

    TalkDtoResponse saveNewTalkByConferenceId(long id, TalkDtoRequest talk);

    List<TalkDtoResponse> getTalksByConferenceId(long id);

}
