package com.example.demo.config;

import io.micrometer.core.aop.CountedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfiguration {

    @Bean
    public CountedAspect countedAspect(MeterRegistry meterRegistry) {
//        meterRegistry.gauge("conference.saved.count", 0);
        return new CountedAspect(meterRegistry);
    }

    /*@Bean
    public MeterRegistryCustomizer meterRegistryCustomizer(MeterRegistry meterRegistry) {
        return meterRegistry1 -> {
            meterRegistry.config()
                    .commonTags("application", "conference-saved-count");
        };
    }*/
}
