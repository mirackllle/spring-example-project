package com.example.demo.domain;

import com.example.demo.model.entity.Talk;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Date;
import java.util.List;

@Data
@Value
@Builder
@RequiredArgsConstructor
public class ConferenceDomain {

    String name;

    String topic;

    Date date;

    int participantsCount;

    List<Talk> talks;

    public void checkFreeDateForConference() {

    }

    /*private void checkFreeDateForConference(Conference conferenceIncome) {
        if(repository.findByDate(conferenceIncome.getDate()) != null) {
            throw new ConferenceDuplicateException("Conference is exist!");
        };
    }

    private void validateConferenceOnDuplicate(Conference conferenceIncome) {
        if (repository.findByNameAndTopicAndDate(conferenceIncome.getName(), conferenceIncome.getTopic(),
                conferenceIncome.getDate()) != null) {
            throw new ConferenceDuplicateException("Conference is exist!");
        };
    }*/
}
