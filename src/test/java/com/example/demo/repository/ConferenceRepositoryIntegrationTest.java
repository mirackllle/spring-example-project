package com.example.demo.repository;

import com.example.demo.model.entity.Conference;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DBRider
@ActiveProfiles("rep-integration-test")
public class ConferenceRepositoryIntegrationTest {

    @Autowired
    public ConferenceRepository conferenceRepository;

    @Test
    @DataSet({"datasets/conferenceDataRider.yml"})
    void findAllConferenceShouldGetAllConference() {
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertEquals(conferenceList.get(0).getId(), 1L);
    }

}
