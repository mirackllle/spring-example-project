package com.example.demo.repository;

import com.example.demo.model.entity.Talk;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DBRider
@ActiveProfiles("rep-integration-test")
public class TalkRepositoryIntegrationTest {

    @Autowired
    TalkRepository repository;

    @Test
    @DataSet({"datasets/conferenceDataRider.yml"})
    void countByConferenceIdAndSpeakerNameShouldReturnNumber() {
        assertEquals(repository.countByConferenceIdAndSpeakerName(1, "talk speaker name"), 1);
    }

    @Test
    @DataSet({"datasets/conferenceDataRider.yml"})
    void getByConferenceIdAndNameShouldReturnTalk() {
        Talk talk = repository.getByConferenceIdAndName(1, "talk test name");
        assertEquals(talk.getName(), "talk test name");
    }
}
