package com.example.demo.controller;


import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConferenceDuplicateException;
import com.example.demo.model.dto.*;
import com.example.demo.service.ConferenceService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("ConferenceControllerTest should ...")
@WebMvcTest
class ConferenceControllerTest {
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ConferenceService service;
    private ConferenceDtoRequest conferenceDto;

    private static String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot create json from = " + obj);
        }
    }

    @BeforeEach
    void setUp() throws ParseException {
        Date date = format.parse("2022-07-15 12:00");
        conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(date)
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(101)
                .build();
    }

    @Test
    void createNewConference() throws Exception {
        //given
        Date date = format.parse("2022-07-15 12:00");
        ConferenceDtoResponse conferenceDomain = ConferenceDtoResponse.builder()
                .conferenceDate(date)
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(101)
                .build();
        //when
        doAnswer((x) -> null).when(service).saveConference(any());
        //then
        mockMvc.perform(MockMvcRequestBuilders
                .post("/conferences")
                .content(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void createNewConference_throwException_whenTryToSaveDuplicate() throws Exception {
        //given
        Date date = format.parse("2022-07-15 12:00");
        ConferenceDtoResponse conferenceDomain = ConferenceDtoResponse.builder()
                .conferenceDate(date)
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(101)
                .build();
        //when
        doThrow(new ConferenceDuplicateException("Conference exists in DB")).when(service).saveConference(any());
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences")
                .content(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$").value("Conference exists in DB"));
    }

    @Test
    void createNewConference_throwException_whenSaveConferenceWithOmittedParams() throws Exception {
        //given
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
//                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(101)
                .build();
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences")
                .content(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createNewConference_throwException_whenSaveConferenceWithParticipantsLowThan100() throws Exception {
        //given
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(11)
                .build();
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences")
                .content(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createNewConference_throwException_whenSaveConferenceWithDateInPast() throws Exception {
        //given
        Date date = format.parse("2000-07-15 12:00");
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(date)
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(11)
                .build();
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences")
                .content(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createNewTalkShouldCreateTalk() throws Exception {
        //given
        TalkDtoRequest mockTalkReq = getMockTalkReq();
        TalkDtoResponse talkDtoResp = getMockTalkResp();
        //when
        when(service.saveNewTalkByConferenceId(1L, mockTalkReq)).thenReturn(talkDtoResp);
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences/1/talk")
                .content(asJsonString(mockTalkReq))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void createNewTalkShouldThrowBadRequestException() throws Exception {
        //given
        TalkDtoRequest mockTalkReq = getMockTalkReq();
        TalkDtoResponse talkDtoResp = getMockTalkResp();
        //when
        doThrow(new BadRequestException("Speaker already has 3 talks")).when(service).saveNewTalkByConferenceId(1L, mockTalkReq);
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/conferences/1/talk")
                .content(asJsonString(mockTalkReq))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getTalksByConfIdShouldReturnListOfTalks() throws Exception {
        TalkDtoResponse talkDtoResp = getMockTalkResp();
        List<TalkDtoResponse> talkDtoResponseList = new ArrayList<>();
        talkDtoResponseList.add(talkDtoResp);
        when(service.getTalksByConferenceId(1L)).thenReturn(talkDtoResponseList);
        mockMvc.perform(MockMvcRequestBuilders.get("/conferences/1/talk")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private TalkDtoRequest getMockTalkReq() {
        TalkTypeDto talkType = new TalkTypeDto(1, "Mock Talk");
        return new TalkDtoRequest("Talk name", "Talk desc", "Talk Speaker", talkType);
    }

    private TalkDtoResponse getMockTalkResp() {
        TalkTypeDto talkType = new TalkTypeDto(1, "Mock Talk");
        return new TalkDtoResponse(1L, "Talk name", "Talk desc", "Talk Speaker", talkType);
    }
}
