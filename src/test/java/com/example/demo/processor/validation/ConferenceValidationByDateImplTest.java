package com.example.demo.processor.validation;

import com.example.demo.model.entity.Conference;
import com.example.demo.repository.ConferenceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@DisplayName("ConferenceValidationByDateImpl should ...")
@ExtendWith(MockitoExtension.class)
class ConferenceValidationByDateImplTest {
//    @Mock
//    private ConferenceRepository conferenceRepository;
//
//    @BeforeEach
//    void setUp() {
//        conferenceValidationByDate = new ConferenceValidationByDateImpl(conferenceRepository);
//    }
//
//    @DisplayName(" success find duplicate by Date ")
//    @Test
//    void findDuplicate() {
//        //given
//        Conference conferenceDao = new Conference();
//        conferenceDao.setDate(Date.from(Instant.now()));
//        //when
//        when(conferenceRepository.findByDate(any(Date.class))).thenReturn(conferenceDao);
//        //then
//        Conference duplicate = conferenceValidationByDate.findDuplicate(conferenceDao);
//        assertNotNull(duplicate);
//    }
//
//    @DisplayName(" duplicate do not exist by Date ")
//    @Test
//    void findDuplicate_duplicateDoNotExist() {
//        //given
//        Conference conferenceDao = new Conference();
//        conferenceDao.setDate(Date.from(Instant.now()));
//        //when
//        when(conferenceRepository.findByDate(any(Date.class))).thenReturn(null);
//        //then
//        Conference duplicate = conferenceValidationByDate.findDuplicate(conferenceDao);
//        assertNull(duplicate);
//    }
}
