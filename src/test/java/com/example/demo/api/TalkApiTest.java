package com.example.demo.api;

import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static io.restassured.RestAssured.*;

public class TalkApiTest extends AbstractApiTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");


    private static String asJsonString(Object obj) {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot create json from = " + obj);
        }
    }

    @DynamicPropertySource
    static void postgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
    }



    @Test()
    void shouldCreateNewTalkForConference() throws ParseException, IOException {
        Long confId = saveConferenceSuccess();
        given()
                .contentType("application/json")
                .body(getNewTalkJSON())
        .when()
                .post(URI.create("/conferences/" + confId + "/talk"))
        .then()
                .statusCode(201)
                .extract();
        deleteConf(confId);
    }

    private Long saveConferenceSuccess() throws ParseException, IOException {
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(format.parse("2022-07-15 12:00:00"))
                .topic("Topic new")
                .name("JEE conf")
                .participantsCount(101)
                .build();

        String response = with()
                .body(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post(URI.create("/conferences"))
                .getBody()
                .asString();
        return objectMapper.readValue(response, ConferenceDtoResponse.class).getId();
    }

    private void deleteConf(Long id) {
        when().delete(URI.create("/conferences/" + id)).then();
    }

    private String getNewTalkJSON() {
        return """
                {
                \t"name": "talk name",
                \t"description": "talk desc",
                \t"speakerName": "talk speaker",
                \t"talkType": {
                \t\t"id": 1,
                \t\t"name": "Talk type"
                \t}
                }""";
    }
}
