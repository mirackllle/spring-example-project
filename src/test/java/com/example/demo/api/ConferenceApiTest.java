package com.example.demo.api;

import com.example.demo.model.dto.ConferenceDtoRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;
import static org.hamcrest.Matchers.hasItems;

@SqlGroup({
        @Sql(
                value = {"/testData/conference/testConferenceData.sql"},
                executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
        ),
        @Sql(
                value = {"/testData/conference/deleteConferenceData.sql"},
                executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD
        )
})
public class ConferenceApiTest extends AbstractApiTest {
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static String asJsonString(Object obj) {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot create json from = " + obj);
        }
    }

    @DynamicPropertySource
    static void postgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
    }

    @Test
//    @DataSet(value = "default-conferences.xml", strategy = SeedStrategy.REFRESH)
    void allConferencesFromDatabaseAreAvailableOnWeb() {
        given()
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .get(URI.create("/conferences"))
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body("name", hasItems("jee conf test", "jee conf test 2"));
    }

    @Test
//    @DataSet(value = "default-conferences.xml", strategy = SeedStrategy.REFRESH)
    void saveConferenceSuccess() throws ParseException {
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(format.parse("2022-07-15 12:00:00"))
                .topic("Topic new 12")
                .name("JEE conf 12")
                .participantsCount(101)
                .build();

        with()
                .body(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post(URI.create("/conferences"))
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
//                .body("name", hasItems("JEE conf"))
        ;
    }

    @Test
    void saveConferenceFailed_confHasExist() throws ParseException {
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(format.parse("2022-07-15 12:00"))
                .topic("topic name test")
                .name("jee conf test")
                .participantsCount(111)
                .build();

        with()
                .body(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post(URI.create("/conferences"))
                .then()
                .statusCode(HttpStatus.SC_CONFLICT)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
        ;
    }

    @Test
    void saveConferenceFailed_confHasCountParticipantBelowMinimumRequired() throws ParseException {
        ConferenceDtoRequest conferenceDto = ConferenceDtoRequest.builder()
                .conferenceDate(format.parse("2022-07-15 12:00"))
                .topic("topic name test 3")
                .name("jee conf test 3")
                .participantsCount(11)
                .build();

        with()
                .body(asJsonString(conferenceDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .when()
                .post(URI.create("/conferences"))
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ;
    }
}
