package com.example.demo.service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConferenceDuplicateException;
import com.example.demo.model.dto.ConferenceDtoRequest;
import com.example.demo.model.dto.ConferenceDtoResponse;
import com.example.demo.model.dto.TalkDtoRequest;
import com.example.demo.model.dto.TalkTypeDto;
import com.example.demo.model.entity.Conference;
import com.example.demo.model.entity.Talk;
import com.example.demo.model.entity.TalkType;
import com.example.demo.model.mapper.ConferenceMapper;
import com.example.demo.repository.ConferenceRepository;
import com.example.demo.repository.TalkRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@DisplayName(" ConferenceServiceImpl should ... ")
@ExtendWith(MockitoExtension.class)
class ConferenceServiceImplTest {

    @Mock
    private ConferenceRepository conferenceRepository;
    @Mock
    TalkRepository talkRepository;
    @Mock
    ApplicationEventPublisher eventPublisher;

    @Spy
    private final ConferenceMapper conferenceMapper = Mappers.getMapper(ConferenceMapper.class);
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    @InjectMocks
    private ConferenceServiceImpl conferenceService;
    private Conference conferenceMock;
    private ConferenceDtoResponse conferenceDto;
    private ConferenceDtoRequest conferenceDtoRequest;

    @BeforeEach
    void setUp() throws ParseException {
        conferenceMock = new Conference();
        conferenceMock.setId(1L);
        conferenceMock.setName("JEE");
        conferenceMock.setTopic("How to");
        conferenceMock.setParticipantsCount(1111);
        conferenceMock.setDate(Date.from(Instant.now()));

        Date date = format.parse("2022-07-15 12:00");
        conferenceDto = ConferenceDtoResponse.builder()
                .conferenceDate(date)
                .name("JEE conf 2022")
                .topic("How to develop with SpringBoot on right way!")
                .participantsCount(111)
                .build();

        conferenceDtoRequest = ConferenceDtoRequest.builder()
                .name("JEE")
                .topic("Conf Topic")
                .conferenceDate(date)
                .participantsCount(111)
                .build();

    }


    @DisplayName(" success save conf ")
    @Test
    void saveConference() {
        //given
        //when
        when(conferenceMapper.mapDtoRequestToEntity(any())).thenReturn(conferenceMock);
        when(conferenceRepository.save(any())).thenReturn(conferenceMock);
        conferenceService.saveConference(conferenceDtoRequest);
        //then
        verify(conferenceRepository, times(1)).save(any());
    }

    @DisplayName(" do not save if conf exists ")
    @Test
    void doNotSave_whenConfIsExists() {
        //given
        //when //then
        when(conferenceMapper.mapDtoRequestToEntity(any())).thenReturn(conferenceMock);
        when(conferenceRepository.findByNameAndTopic(anyString(), anyString())).thenReturn(new Conference());
        assertThrows(ConferenceDuplicateException.class,
                () -> conferenceService.saveConference(conferenceDtoRequest),
                "Conference is exist!");
        verify(conferenceRepository, times(0)).save(any());
    }

    @DisplayName(" do not save if conf date is occupied ")
    @Test
    void doNotSave_whenConfDateIsOccupied() {
        //given
        //when //then
        when(conferenceMapper.mapDtoRequestToEntity(any())).thenReturn(conferenceMock);
        when(conferenceRepository.findByDate(any())).thenReturn(new Conference());
        assertThrows(ConferenceDuplicateException.class,
                () -> conferenceService.saveConference(conferenceDtoRequest),
                "Conference is exist!");
        verify(conferenceRepository, times(0)).save(any());
    }

    @Test
    void createNewTalk_throwException_whenTooMuchTalks() {
        Conference mockConf = getMockConference();
        mockConf.getTalks().add(mockConf.getTalks().get(0));
        mockConf.getTalks().add(mockConf.getTalks().get(0));
        when(conferenceRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(mockConf));
        Mockito.when(talkRepository.countByConferenceIdAndSpeakerName(Mockito.anyLong(), Mockito.anyString()))
                .thenReturn(3);
        TalkTypeDto talkType = new TalkTypeDto(1, "Mock Talk");
        TalkDtoRequest talkDto = new TalkDtoRequest("Talk name", "Talk desc", "Talk Speaker", talkType);
        assertThrows(BadRequestException.class,
                () -> conferenceService.saveNewTalkByConferenceId(2, talkDto),
                "Speaker already has 3 talks");
    }

    @Test
    void createNewTalk_throwException_whenNotUniqueName() {
        Conference mockConf = getMockConference();
        when(conferenceRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(mockConf));
        Mockito.when(talkRepository.getByConferenceIdAndName(Mockito.anyLong(), Mockito.anyString()))
                .thenReturn(mockConf.getTalks().get(0));
        TalkTypeDto talkType = new TalkTypeDto(1, "Mock Talk");
        TalkDtoRequest talkDto = new TalkDtoRequest("Talk name", "Talk desc", "Talk Speaker", talkType);
        assertThrows(ConferenceDuplicateException.class,
                () -> conferenceService.saveNewTalkByConferenceId(2, talkDto),
                "Talk must have unique name");
    }

    @Test
    void createNewTalk_throwException_whenNotEnoughTime() {
        Conference mockConf = getMockConference();
        when(conferenceRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(mockConf));
        TalkTypeDto talkType = new TalkTypeDto(1, "Mock Talk");
        TalkDtoRequest talkDto = new TalkDtoRequest("Talk name1", "Talk desc", "Talk Speaker", talkType);
        assertThrows(BadRequestException.class,
                () -> conferenceService.saveNewTalkByConferenceId(2, talkDto),
                "Left less than 30 days, you can't add new talk");
    }

    @Test
    void createNewTalk_successfullyCreated() {
        Conference mockConf = getMockConference();
        mockConf.setDate(Date.from(Instant.parse("2022-11-30T18:35:24.00Z")));
        when(conferenceRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(mockConf));
        when(talkRepository.save(Mockito.any(Talk.class)))
                .thenReturn(mockConf.getTalks().get(0));
        TalkTypeDto talkTypeDto = new TalkTypeDto(1, "Mock Talk");
        TalkDtoRequest talkDto = new TalkDtoRequest("Talk name", "Talk desc", "Talk Speaker", talkTypeDto);
        var talkDtoResponse = conferenceService.saveNewTalkByConferenceId(2, talkDto);
        assertEquals(talkDtoResponse.getName(), talkDto.getName());
    }

    private Conference getMockConference() {
        TalkType talkType = new TalkType(1, "Mock Talk");
        Talk talk = new Talk(1L, "Talk name", "Talk desc", "Talk Speaker", talkType, 1L);
        List<Talk> list = new ArrayList<>();
        list.add(talk);
        Conference conference = new Conference();
        conference.setId(1L);
        conference.setDate(Date.from(Instant.now()));
        conference.setName("Test conference");
        conference.setParticipantsCount(101);
        conference.setTopic("Conference topic");
        conference.setTalks(list);
        return conference;
    }
}
